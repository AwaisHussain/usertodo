export interface TaskList {
  id: string;
  name: string;
  checked: boolean;
}
export interface Task {
  id: string;
  name: string;
}
