import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import styled from "styled-components";
import Home from "./components/todo";
import TodoBulkDelete from "./components/todo/todoBulkDelete";
import CreateTodo from "./components/todo/todoForm";
import TodoList from "./components/todo/todoList";
const AppWrapper = styled.div`
  max-width: 600px;
  margin: 10% auto;
  padding: 20px;
`;

const App: React.FC = () => {
  return (
    <Router>
      <AppWrapper>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/list-tasks" element={<TodoList />} />
          <Route path="/create-task" element={<CreateTodo />} />
          <Route path="/bulk-delete" element={<TodoBulkDelete />} />
        </Routes>
      </AppWrapper>
    </Router>
  );
};

export default App;
