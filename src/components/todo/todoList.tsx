import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { styled } from "styled-components";
import { Button } from "../elements/Button";
import Card from "../elements/Card";
import { Task } from "../../Types/types";

const BtnWrapper = styled.div`
  text-align: end;
  margin-bottom: 20px;
`;
const EmptyContainer = styled.div`
  text-align: center;
`;
const TodoList: React.FC = () => {
  const [tasks, setTasks] = useState<Task[]>([]);
  const navigate = useNavigate();
  React.useEffect(() => {
    const storedTasks: Task[] = JSON.parse(
      localStorage.getItem("tasksList") || "[]"
    );
    setTasks(storedTasks);
  }, []);
  const handleCreateRedirect = () => {
    navigate("/create-task");
  };

  const handleDeleteRedirect = () => {
    navigate("/bulk-delete");
  };

  return (
    <div>
      <h2>Tasks</h2>
      <BtnWrapper>
        <Button color="primary" onClick={handleCreateRedirect}>
          + Add Task
        </Button>
        {tasks?.length > 0 ? (
          <Button
            color="danger"
            onClick={handleDeleteRedirect}
            style={{ marginLeft: "5px" }}
          >
            Bulk Delete
          </Button>
        ) : null}
      </BtnWrapper>
      {tasks?.map((task, index) => (
        <Card key={index} task={task.name} />
      ))}

      {tasks?.length < 1 ? (
        <EmptyContainer>
          <p> No Task</p>
        </EmptyContainer>
      ) : null}
    </div>
  );
};

export default TodoList;
