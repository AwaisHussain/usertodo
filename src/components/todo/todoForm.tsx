import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { BackButton } from "../elements/BackBtn";
import { Button } from "../elements/Button";
import { FlexBtnWrapper } from "../elements/FlexBtns";
import Input from "../elements/Input";
import { Task } from "../../Types/types";

const CreateTodo: React.FC = () => {
  const navigate = useNavigate();
  const [task, setTask] = useState<string>("");

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTask(event.target.value);
  };

  const handleAdd = () => {
    if (task.trim() !== "") {
      const storedTasks: Task[] = JSON.parse(
        localStorage.getItem("tasksList") || "[]"
      );
      const newTask = {
        id: Date.now().toString(),
        name: task,
      };
      storedTasks.push(newTask);
      localStorage.setItem("tasksList", JSON.stringify(storedTasks));
      navigate("/list-tasks");
    }
  };
  const handleBack = () => {
    navigate("/list-tasks");
  };
  return (
    <div>
      <h2>Enter Your Task</h2>
      <Input value={task} onChange={handleChange} />
      <FlexBtnWrapper>
        <div onClick={handleBack}>
          <BackButton />
        </div>
        <Button color="primary" onClick={handleAdd}>
          Create Task
        </Button>
      </FlexBtnWrapper>
    </div>
  );
};

export default CreateTodo;
