import React, { useState } from "react";
import Card from "../elements/Card";
import { Button } from "../elements/Button";
import { useNavigate } from "react-router-dom";
import { BackButton } from "../elements/BackBtn";
import { FlexBtnWrapper } from "../elements/FlexBtns";
import { ConfirmationAlert } from "../common/confirmationPopup";
import { Task, TaskList } from "../../Types/types";

const TodoBulkDelete: React.FC = () => {
  const navigate = useNavigate();
  const [showConfirmation, setShowConfirmation] = useState(false);
  const [tasks, setTasks] = useState<TaskList[]>([]);
  React.useEffect(() => {
    const storedTasks: Task[] = JSON.parse(
      localStorage.getItem("tasksList") || "[]"
    );
    setTasks(
      storedTasks?.map((task: Task, index: number) => ({
        id: task?.id,
        name: task?.name,
        checked: false,
      }))
    );
  }, []);

  const handleDelete = () => {
    const filteredTasks = tasks
      .filter((task) => !task.checked)
      .map(({ checked, ...rest }) => rest);
    localStorage.setItem("tasksList", JSON.stringify(filteredTasks));
    setTasks(
      filteredTasks?.map((task: Task, index: number) => ({
        id: task?.id,
        name: task?.name,
        checked: false,
      }))
    );
    handleCancel();
  };

  const handleCheckboxChange = (
    event: React.ChangeEvent<HTMLInputElement>,
    taskId: string
  ) => {
    const checked = event.target.checked;
    setTasks((prevTasks) =>
      prevTasks.map((task) =>
        task.id === taskId ? { ...task, checked } : task
      )
    );
  };
  const handleBack = () => {
    navigate("/list-tasks");
  };

  const handleShowConfirmation = () => {
    setShowConfirmation(true);
  };

  const handleCancel = () => {
    setShowConfirmation(false);
  };
  return (
    <div>
      {showConfirmation ? (
        <ConfirmationAlert
          message="Are you sure you want to Delete?"
          onCancel={handleCancel}
          onConfirm={handleDelete}
          show={showConfirmation}
        />
      ) : null}
      <h2>Bulk Delete Tasks</h2>
      <FlexBtnWrapper>
        <div onClick={handleBack}>
          <BackButton />
        </div>
        {tasks?.filter((item: TaskList) => item?.checked === true).length ? (
          <Button color="danger" onClick={handleShowConfirmation}>
            Delete
          </Button>
        ) : null}
      </FlexBtnWrapper>
      {tasks?.map((task) => (
        <Card key={task.id} task={task.name}>
          <label>
            <input
              type="checkbox"
              checked={task.checked}
              onChange={(e) => handleCheckboxChange(e, task.id)}
            />
          </label>
        </Card>
      ))}
    </div>
  );
};

export default TodoBulkDelete;
