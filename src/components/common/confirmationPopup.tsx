import React from "react";
import styled from "styled-components";
import { Button } from "../elements/Button";
import { FlexBtnWrapper } from "../elements/FlexBtns";

interface ConfirmationAlertProps {
  message: string;
  onCancel: () => void;
  onConfirm: () => void;
  show: boolean;
}

const ConfirmationAlertWrapper = styled.div<{ show: boolean }>`
  display: ${(props) => (props.show ? "block" : "none")};
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 999;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ConfirmationBox = styled.div`
  background-color: white;
  padding: 20px;
  border-radius: 5px;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
  text-align: center;
`;

export const ConfirmationAlert: React.FC<ConfirmationAlertProps> = ({
  message,
  onCancel,
  onConfirm,
  show,
}) => {
  return (
    <ConfirmationAlertWrapper show={show}>
      <ConfirmationBox>
        <p>{message}</p>
        <FlexBtnWrapper>
          <Button color="primary" onClick={onCancel}>
            Cancel
          </Button>
          <Button color="danger" onClick={onConfirm}>
            Confirm
          </Button>
        </FlexBtnWrapper>
      </ConfirmationBox>
    </ConfirmationAlertWrapper>
  );
};
