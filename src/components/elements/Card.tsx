import React, { ReactNode } from "react";
import styled from "styled-components";

const Card = styled.div`
  display: flex;
  align-items: center;
  border: 1px solid #ccc;
  padding: 10px;
  margin: 20px 0;
  background: #fff;
  box-shadow: 0 8px 32px rgba(80, 80, 80, 0.16);
  border-radius: 8px;
`;

interface TaskCardProps {
  task: string;
  children?: ReactNode;
}

const TaskCard: React.FC<TaskCardProps> = ({ task, children }) => {
  return (
    <Card>
      {children}
      <p>{task}</p>
    </Card>
  );
};

export default TaskCard;
