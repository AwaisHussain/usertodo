import React from "react";
import styled from "styled-components";

export const FlexBtnWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;
interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {}

export const BackButton: React.FC<ButtonProps> = ({ children }) => {
  return <FlexBtnWrapper>{children}</FlexBtnWrapper>;
};
