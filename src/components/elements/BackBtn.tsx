import React from "react";
import styled from "styled-components";

const BackBtn = styled.div`
  color: #007bff;
  cursor: pointer;
  margin-top: 4px;
`;

export const BackButton: React.FC = () => {
  return <BackBtn>&#8592; Back to List</BackBtn>;
};
